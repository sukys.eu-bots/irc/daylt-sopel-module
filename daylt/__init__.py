# coding=utf-8
"""
daylt bot
"""

from typing import List, Dict
from datetime import datetime, timedelta
import sopel.module
import sopel.formatting
import requests
import re
from bs4 import BeautifulSoup


PAGE_URL = 'https://day.lt/'
DAY_URL = PAGE_URL + 'diena/'


class Day:
    def __init__(self):
        self.date = datetime.today()
        self.names = []
        self.sun = {}
        self.weather = {}
        self.fests = []

    def get_formatted_names(self) -> str:
        if not self.names:
            return sopel.formatting.bold('Šiandien bevardžių diena :--D')

        return sopel.formatting.bold('Šiandien vardadienį švenčia') + ': ' + ', '.join(self.names)

    def get_formatted_reason(self) -> str:
        if not self.fests:
            return 'ramei'

        return 'Proga ' + sopel.formatting.bold('išgerti') + ', nes yra ką švęst: ' + ', '.join(self.fests)

    def get_formatted_day(self) -> List[str]:
        lines = []
        message = sopel.formatting.bold(self.date.strftime("%Y-%m-%d") + ': ')

        # sun information
        message += 'saulė ' + self.sun['sunrise'] + ' - ' + self.sun['sunset'] + '. '

        # weather information
        message += 'Dieną ' + self.weather['day'] + ', vakare ' + self.weather['night'] + ', ' + self.weather['sky'] + '.'

        lines.append(message)

        if self.fests:
            lines.append(str(len(self.fests)) + ' šventės: ' + ", ".join(self.fests))

        lines.append(self.get_formatted_names())

        return lines


    def parse_names(self, html_contents: str) -> List[str]:
        """Parses out names of the day from day.lt

        Arguments:
            html_contents {str} -- contents of the page

        Returns:
            list -- list of names, elements should be of type string
        """
        names = []
        soup = BeautifulSoup(html_contents, 'html.parser')
        for a in soup.find('p', {'class': 'vardadieniai'}).findAll('a'):
            name = a.text.strip()
            if name:
                names.append(name)

        return names

    def parse_sun_info(self, html_contents: str) -> dict:
        """Parses sun information.

        Arguments:
            html_contents {str} -- contents of the page

        Returns:
            dict -- information about sun times in the following format: {'sunrise': '', 'sunset': ''}
        """
        soup = BeautifulSoup(html_contents, 'html.parser')
        plain_info = soup.find('p', {'title': 'Saulė'}).text
        # 0: sunrise, 1: sunset, 2: day length
        info_tokens = plain_info.strip().splitlines()
        return {'sunrise': info_tokens[0], 'sunset': info_tokens[1]}

    def parse_weather_info(self, html_contents: str) -> dict:
        """Parses weather information.

        Arguments:
            html_contents {str} -- contents of the page

        Returns:
            dict -- information about temperature in celsius in the following format {'day': '', 'night': '', 'sky': ''}
        """
        soup = BeautifulSoup(html_contents, 'html.parser')
        plain_info = soup.find('p', {'id': 'orai'})
        weather_plain = plain_info.find(
            'strong', string='Šiandien').nextSibling.nextSibling.text
        matches = re.match('(\S+)°C / (\S+)°C, (\S+)', weather_plain)
        return {'day': matches.group(1), 'night': matches.group(2), 'sky': matches.group(3)}

    def get_fests(self, html_contents: str) -> list:
        """Parses festivities of the day.

        Arguments:
            html_contents {str} -- contents of the page

        Returns:
            list -- list of full names of festivities ['internantional kebap day', 'beer day', ...]
        """
        soup = BeautifulSoup(html_contents, 'html.parser')
        plain_info = soup.find('p', {'class': 'sventes'})

        if plain_info is None:  # no fests for today :-(
            return []

        return list(map(lambda item: item.text, plain_info.findAll('span')))

    def parse(self, html_contents: str, date: datetime):
        self.date = date
        self.names = self.parse_names(html_contents)
        self.sun = self.parse_sun_info(html_contents)
        self.weather = self.parse_weather_info(html_contents)
        self.fests = self.get_fests(html_contents)


def get_page(url: str) -> str:
    """Retrieves html contents of day.lt day's information.

    Returns:
        str -- HTML contents
    """
    page = requests.get(url)
    page.encoding = 'windows-1257'
    return page.text

def get_day_page(day: datetime) -> str:
    date = day.strftime("%Y.%m.%d")
    url = DAY_URL + date 
    return get_page(url)

@sopel.module.commands('siandien')
def siandien(bot, trigger):
    day = Day()
    date = datetime.today()
    day.parse(get_day_page(date), date)
    messages = day.get_formatted_day()
    for message in messages:
        bot.say(message)

@sopel.module.commands('rytoj')
def rytoj(bot, trigger):
    day = Day()
    date = datetime.today() + timedelta(days=1)
    day.parse(get_day_page(date), date)
    messages = day.get_formatted_day()
    for message in messages:
        bot.say(message)

@sopel.module.commands('diena')
def diena(bot, trigger):
    day = Day()
    try:
        date = datetime.strptime(trigger.group(2), '%Y-%m-%d')
        day.parse(get_day_page(date), date)
        messages = day.get_formatted_day()
        for message in messages:
            bot.say(message)
    except ValueError:
        bot.say('Gali normaliai įvest datą?')

@sopel.module.commands('proga')
def proga(bot, trigger):
    day = Day()
    date = datetime.today()
    while not day.fests:
        day.parse(get_day_page(date), date)
        date += timedelta(days=1)
        
    date_diff = date - datetime.today()
    if date_diff.days > 0:
        bot.say('Proga tik už ' + str(date_diff.days) + ' d..')
    bot.say(day.get_formatted_reason())
    